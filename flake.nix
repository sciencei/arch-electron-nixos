{
  inputs = {
  };

  description = "Electron as compiled by arch linux";

  outputs = all@{ self, nixpkgs }: {
    
    packages.x86_64-linux =
      let
        lib = nixpkgs.lib;
        pkgs = nixpkgs.legacyPackages.x86_64-linux;
      in rec {
        electron =
          let
            version = "16.0.8";
            ffmpegPatched = pkgs.ffmpeg_5.override { patches = [./ffmpeg-chromium.patch]; };
            libjpeg_turbo8 = pkgs.libjpeg_turbo.override { enableJpeg8 = true; };
            electronLibPath = with lib; with pkgs; makeLibraryPath (
              [ icu70
                harfbuzz 
                libevent
                libjpeg_turbo8
                libopus
                ffmpegPatched
                libGL
                libpng
                libwebp
                wayland
                re2
                flac
                fontconfig
                freetype
                libxml2
                pulseaudio
                libxslt
                snappy
                zlib
                minizip
                c-ares
                libnghttp2

                libuuid
                at-spi2-atk
                at-spi2-core
                libappindicator-gtk3

                libdrm
                mesa
                
                libxkbcommon

                xorg.libxshmfence

              ]
            );
          in pkgs.stdenv.mkDerivation {
            inherit version;
            pname = "electron";
            description = "Cross platform desktop application shell";
            homepage = "https://github.com/electron/electron";
            # not wrapped in a top level folder so we can't do it as an input, apparently
            src = pkgs.fetchzip {
              name = "arch-electron";
              url = "https://archive.archlinux.org/packages/e/electron/electron-${version}-2-x86_64.pkg.tar.zst";
              sha256 = "GQGOnjy5/59Il+bgnU7IW5VpjyqO68jbBEiuhKvTwRs=";
              nativeBuildInputs = [ pkgs.zstd ];
            };

            buildInputs = with pkgs; [ glibc gtk3 ];

            nativeBuildInputs = with pkgs; [
              unzip
              makeWrapper
              wrapGAppsHook
            ];
            dontWrapGApps = true; # electron is in lib, we need to wrap it manually

            dontUnpack = true;
            dontBuild = true;
            installPhase = ''
              mkdir $out
              mkdir $out/bin
              cp -r $src/lib $out
              cp -r $src/share $out
              ln -s $out/lib/electron/electron $out/bin
            '';

            postFixup = ''
              patchelf \
                --set-interpreter "$(cat $NIX_CC/nix-support/dynamic-linker)" \
                --set-rpath "${pkgs.atomEnv.libPath}:${electronLibPath}:$out/lib/electron" \
                $out/lib/electron/electron \
                ${lib.optionalString (lib.versionAtLeast version "15.0.0") "$out/lib/electron/chrome_crashpad_handler" }

              wrapProgram $out/lib/electron/electron "''${gappsWrapperArgs[@]}"
            '';
        };
    };
    defaultPackage.x86_64-linux = self.packages.x86_64-linux.electron;
    apps.x86_64-linux.electron = {
      type = "app";
      program = self.packages.x86_64-linux.electron;
    };
  };
}
